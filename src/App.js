import React, { Component } from "react";
import TodoList from './components/todo-list/TodoList.js'
import { Route, NavLink } from 'react-router-dom'
import {connect} from 'react-redux'
import {addTodo, clearCompletedTodos} from './actions'

class App extends Component {
 
  handleKeyDown = (event) => {
    if (event.key === "Enter" && event.target.value !== '') {
      console.log("entered!");
      this.props.addTodo(event.target.value)
      event.target.value = ''
    }
  };

  
  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>To-Dos</h1>
          <input
            className="new-todo"
            placeholder="What needs to be done?"
            autoFocus
            onKeyDown={this.handleKeyDown}
          />
        </header>

        <Route 
        exact path="/"
        render ={() => (
        <TodoList
          todos={this.props.todos}
          // handleComplete={this.handleComplete}
          // handleDelete={this.handleDelete}
          // handleDeleteAll={this.handleDeleteAll}
        />
        )}
        />
          <Route 
         path="/active"
        render ={() => (
        <TodoList
          todos={this.props.todos.filter(todo => todo.completed === false)}
          // handleComplete={this.handleComplete}
          // handleDelete={this.handleDelete}
          // handleDeleteAll={this.handleDeleteAll}
        />
        )}
        />
          <Route 
        path="/completed"
        render ={() => (
        <TodoList
          todos={this.props.todos.filter(todo => todo.completed === true)}
          // handleComplete={this.handleComplete}
          // handleDelete={this.handleDelete}
          // handleDeleteAll={this.handleDeleteAll}
        />
        )}
        />
        
        <footer className="footer">
         
          <ul className="filters">
            <li>
              <NavLink exact to="/" activeClassName="selected">All</NavLink>
            </li>
            <li>
            <NavLink to="/active" activeClassName="selected">Active</NavLink>
            </li>
            <li>
            <NavLink to="/completed" activeClassName="selected">Completed</NavLink>
            </li>
          </ul>
          <button className="clear-completed" onClick={this.handleDeleteAll}>
            Clear Completed
          </button>
        </footer>
      </section>
    );
  }
}

const mapStateToProps = state => {
  return({todos: state.todos})
}

const mapDispatchToProps = {
  addTodo, clearCompletedTodos
}

export default connect(mapStateToProps, mapDispatchToProps) (App);

/* <span className="todo-count">
<strong>{
  this.state.todos.filter((todo) => todo.completed !== true).length
  }</strong> item(s) left
  </span> */

// handleChange = (event) => {
//   this.setState({ ...this.state, value: event.target.value });
// };


// handleComplete = (id) => {
//   let newTodo = this.state.todos.map((todo) => {
//     if (todo.id === id) {
//       return {
      
//         ...todo,
//         completed: !todo.completed,
//       };
//     }
//     return todo;
//   });
//   this.setState((state) => {
//     return {
//       ...state,
//       todos: newTodo,
//     };
//   });
// };

// handleDelete = (event, id) => {
//   const newTodoList = this.state.todos.filter((todo) => todo.id !== id);
//   this.setState({ todos: newTodoList });
// };


// handleDeleteAll = (event) => {
//   const newTodoList = this.state.todos.filter(
//     (todo) => todo.completed === false
//   );
//   this.setState({ todos: newTodoList });
// };
