import React, { Component }from 'react';

class TodoItem extends Component {
    
    render() {
      return (
        <li className={this.props.completed ? "completed" : ""}>
          <div className="view">
            <input
              className="toggle"
              onChange={this.props.handleComplete}
              type="checkbox"
              checked={this.props.completed}
            />
            {/* onChange allows us to listen to an input's change in value */}
            <label>{this.props.title}</label>
            <button className="destroy" onClick={this.props.handleDelete} />
          </div>
        </li>
      );
    }
  }

  export default TodoItem;