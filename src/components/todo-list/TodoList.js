import React, { Component } from 'react';
import TodoItem from '../todo-item/TodoItem'
import {connect} from 'react-redux'
import {toggleTodo, deleteTodo} from '../../actions'

class TodoList extends Component {
    

    render() {
      return (
        <section className="main">
          <ul className="todo-list">
            {this.props.todos.map((todo) => (
              <TodoItem
                key={todo.id}
                title={todo.title}
                completed={todo.completed}
                handleComplete={(event) => this.props.toggleTodo(todo.id)}
                handleDelete={(event) => this.props.deleteTodo(event, todo.id)}
              />
            ))}
          </ul>
        </section>
      );
    }
  }
  
const mapDispatchToProps = {
 toggleTodo, deleteTodo
}

export default connect(null, mapDispatchToProps)(TodoList);