import { ADD_TODO, DELETE_TODO, TOGGLE_TODO, CLEAR_COMPLETED_TODOS } from './actions'
import todosList from "./todos.json";

let initialState = {
    todos: todosList
}

const todoReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_TODO: 
            return [...state, action.payload]
        ;
        case DELETE_TODO: 
            return [...state.filter(todo => todo.id !== action.id)]
        ;
        case TOGGLE_TODO: 
            return [...state.map(todo =>
                todo.id === action.id ? {...todo, completed: !todo.completed } : todo)]
        ;
        case CLEAR_COMPLETED_TODOS: 
            return [...state.todos.filter((todo) => todo.completed === false)]
        ;
        default:
            return state
    }
};

export default todoReducer;